# Studio2Streamer

![Streamer video](https://gitlab.com/jmgasper/studio2streamer/raw/master/images/studio2.gif?inline=false "Streamer video")

This repo contains some basic ALSA setup info and code that I wrote to run my Studio 2 / Spotify Connect streamer conversion.  Everything here is kind of half-assed because I just got it to work, not necessarily to look pretty.

### Background

I wanted a Spotify Connect streamer for my office, but I didn't want something that looked super modern, had a ton of unnecessarily complex features, or [cost way too much](https://spotify-everywhere.com/collections/hi-fi).  A McIntosh MS500 would be cool, but spending $12,000 on it wasn't an option.  I had a bunch of Raspberry Pis sitting around from various generations and different work projects, so I decided to see if I could mash one of those into a retro case and have it look better and work more simply than anything available to purchase.

### Platform

The general platform setup is as follows:

* Raspberry Pi B+
* [Hifiberry Amp 2](https://www.hifiberry.com/shop/boards/hifiberry-amp2/) 
* Mission M70 bookshelf speakers bought of eBay

### Specific parts

* [OLED display for the front](https://www.banggood.com/0_91-Inch-128x32-IIC-I2C-Blue-OLED-LCD-Display-DIY-Oled-Module-SSD1306-Driver-IC-DC-3_3V-5V-p-1140506.html?rmmds=search&cur_warehouse=CN):  
* [ADC module](https://www.banggood.com/I2C-ADS1115-16-Bit-ADC-4-Channel-Module-With-Programmable-Gain-Amplifier-For-Arduino-RPi-p-1110588.html?rmmds=search&cur_warehouse=CN), to interface with the existing potentiometers: 

### Setup

The goal here was to reuse as much as possible from an original chassis / piece of equipment.  I just kept my eye out on eBay for a few weeks for something small that fit the bill, assuming I could just figure out the fit after I received it and cleaned it out.  I wanted something cheap that had a good retro look.

I purchased the Studio2 off eBay for about $15, with the intent of stripping it out and adding in things that fit.  I didn't have a real clear plan of what that would like when I started.

In general, this is what the inside of the amplifier looked like from eBay.  I stripped everything out, save for the potentiometers on the dials on the front.

I put in a power jack in the back where the power cable used to come in and wired that to the switch in front for On / Off functionality.  The power is fed from a [12v / 60w power supply](https://www.banggood.com/AC-100-240V-to-DC-12V-5A-60W-Power-Supply-Adapter-For-LED-Strip-Light-p-994870.html?rmmds=search&ID=47184&cur_warehouse=CN)

There was a second switch on the front for mono/stereo operation that I didn't care about, so I stripped that out, and I realised that the .91 inch OLED would be a good fit in that spot, so I wired it up and plopped it in.

I stripped the RCA jacks off the back for input and replaced those with some banana jacks and wired those into the Hifiberry Amp output for the sound.

The original light on the front is wired into the 5V rail available on the rPI.  It's not as bright as it was originally, but it works.

A USB jack was added to the back in an original space for an external power jack that just happened to be the right fit.

#### Before

The internals of the amp, before everything was stripped out.

<img src="https://gitlab.com/jmgasper/studio2streamer/raw/master/images/IMG_20190826_112847.jpg?inline=false" alt="Internals before" width="600"/>

#### After

<img src="https://gitlab.com/jmgasper/studio2streamer/raw/master/images/IMG_20190920_161909.jpg?inline=false" alt="Internals after" width="600"/>


<img src="https://gitlab.com/jmgasper/studio2streamer/raw/master/images/IMG_20191031_111243.jpg?inline=false" alt="Front after" width="600"/>


<img src="https://gitlab.com/jmgasper/studio2streamer/raw/master/images/IMG_20191031_111252.jpg?inline=false" alt="Back after" width="600"/>

### OS 

The OS on the rPi is Raspbian, but the modified version from Hifiberry [here](https://www.hifiberry.com/build/download/).

I installed python3 and [raspotify](https://github.com/dtcooper/raspotify) for handling the Spotify Connect work.   One caveat is that I decided not to use the raspotify service, instead using the librespot library directly for a little easier testing.

The [peppyalsa plugin](https://github.com/project-owner/peppyalsa.doc/wiki) was built and installed to allow for the spectrum analyzer on the front.

[pyalsaaudio](https://github.com/larsimmisch/pyalsaaudio) is used to allow for ALSA control via Python:  

### .asoundrc

The biggest hurdle was figuring out how to get *both* the equaliser plugin *and* peppyalsa to work together.  The .asoundrc file in this repo works for me, allowing for both the equaliser to work (allowing us to modify bass and treble), along with the spectrum analyzer info out to FIFO for the little display on the front.

### Code

The code in [studio2.py](https://gitlab.com/jmgasper/studio2streamer/blob/master/studio2.py) handles a few things:

1.  It connects to Spotify with my account key and secret to allow for pulling the current play time and duration of the current track off my account, to show a basic time at the top of the display.  I toyed with showing a progress meter, but it just didn't work with the spectrum analyzer and the limited space on the display.

2.  The code reads the FIFO values from peppyalsa and displays them on the display as a bar chart, with one pixel width for each piece of the spectrum (which is configured for 30 frequency segments)

3.  The code watches for changes to the potentiometers in the front via the ADC and updates ALSA accordingly via pyalsaaudio.  The bass and treble controls handle setting various frequency ranges.  It's very coarse-grained, but it works in a pinch.  Volume and balance are handled as well.  The ADC output isn't a real "linear" value - that's likely attributable to these very old potentiometers, my wiring, etc...   I worked around it in code a bit.  It's not perfect, but it does what I need it to do.

### End result

The end result is that I have a Spotify Connect device on my network called "Office" that I can stream music to directly from Spotify, it draws a cool spectrum analyzer animation on the screen, and I can control volume, balance, bass, and treble using the original knobs of the device.

### Future

I have an I2S microphone coming that I'll eventually install to allow for either Google or Mycroft assistant integration, which will likely lead to a much larger codebase.  The microphone will likely go on the front where the original input selector was, covered by acoustic fabric.

I have some better rotary encoders that could be used to in place of the original potentiometers and allow for more functionality, including skipping tracks right on the device, but that's not something I'm too worried about at the moment.

An audio input would be kind of nice.  I have a USB turntable that I could potentially use here to play records through the rPI.  I haven't tested it yet.

I also have to fill in the back empty space with... something.  